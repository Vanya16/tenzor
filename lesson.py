import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service as FirefoxService
from webdriver_manager.firefox import GeckoDriverManager


@pytest.fixture(scope="module")
def driver():
    # Инициализация драйвера Firefox
    service = FirefoxService(GeckoDriverManager().install())
    driver = webdriver.Firefox(service=service)
    driver.implicitly_wait(10)
    yield driver
    driver.quit()


def test_sbis_tensor_navigation(driver):
    # Шаг 1: Перейти на https://sbis.ru/
    driver.get("https://sbis.ru/")

    # Шаг 2: Перейти в раздел "Контакты"
    contacts_link = driver.find_element(By.LINK_TEXT, "Контакты")
    contacts_link.click()

    # Шаг 3: Найти баннер Тензор и кликнуть по нему
    tensor_banner = driver.find_element(By.CSS_SELECTOR, "a[href='https://tensor.ru/']")
    tensor_banner.click()

    # Переключение на новую вкладку
    driver.switch_to.window(driver.window_handles[-1])

    # Шаг 4: Проверить, что есть блок "Сила в людях"
    assert driver.find_element(By.XPATH, "//*[text()='Сила в людях']")

    # Шаг 5: Перейти в блоке "Сила в людях" в "Подробнее"
    more_button = driver.find_element(By.XPATH, "//a[text()='Подробнее' and @href='/about']")
    more_button.click()

    # Проверка, что открывается https://tensor.ru/about
    assert "about" in driver.current_url

    # Шаг 6: Найти раздел "Работаем" и проверить фотографии
    work_section = driver.find_element(By.XPATH, "//section[contains(@class, 'work')]")
    images = work_section.find_elements(By.TAG_NAME, "img")

    for img in images:
        width = img.get_attribute("width")
        height = img.get_attribute("height")
        assert width == height, f"Image {img.get_attribute('src')} does not have equal width and height"


if __name__ == "__main__":
    pytest.main()
